﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vGame.ECS.Attributes
{
    /// <summary>Class ArtemisEntityTemplate.</summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public sealed class ECSEntityTemplate : Attribute
    {
        /// <summary>Initializes a new instance of the <see cref="ArtemisEntityTemplate"/> class.</summary>
        /// <param name="name">The name.</param>
        public ECSEntityTemplate(string name)
        {
            this.Name = name;
        }

        /// <summary>Gets the name.</summary>
        /// <value>The name.</value>
        public string Name { get; private set; }
    }
}
