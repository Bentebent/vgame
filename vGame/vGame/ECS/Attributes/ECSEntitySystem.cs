﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using vGame.ECS.Manager;

namespace vGame.ECS.Attributes
{
    /// <summary>Class ArtemisEntitySystem.</summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public sealed class ECSEntitySystem : Attribute
    {
        /// <summary>Initializes a new instance of the <see cref="ArtemisEntitySystem"/> class.</summary>
        public ECSEntitySystem()
        {
            this.GameLoopType = GameLoopType.Update;
            this.Layer = 0;
            this.ExecutionType = ExecutionType.Synchronous;
        }

        /// <summary>Gets or sets the type of the game loop.</summary>
        /// <value>The type of the game loop.</value>
        public GameLoopType GameLoopType { get; set; }

        /// <summary>Gets or sets the layer.</summary>
        /// <value>The layer.</value>
        public int Layer { get; set; }

        /// <summary>Gets or sets the type of the execution.</summary>
        /// <value>The type of the execution.</value>
        public ExecutionType ExecutionType { get; set; }
    }
}
