﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;

using vGame.ECS.Utils;

namespace vGame.ECS.Attributes
{

#if METRO
    /// <summary>The application domain class.</summary>
    public sealed class AppDomain
    {
        /// <summary>Initializes static members of the <see cref="AppDomain"/> class.</summary>
        static AppDomain()
        {
            CurrentDomain = new AppDomain();
        }

        /// <summary>Gets the current domain.</summary>
        /// <value>The current domain.</value>
        public static AppDomain CurrentDomain { get; private set; }

        /// <summary>Gets the assemblies.</summary>
        /// <returns>The IEnumerable{Assembly}.</returns>
        public IEnumerable<Assembly> GetAssemblies()
        {
            return this.GetAssemblyListAsync().Result;
        }

        /// <summary>Gets the assembly list async.</summary>
        /// <returns>System.Threading.Tasks.Task{IEnumerable{Assembly}}.</returns>
        private async Task<IEnumerable<Assembly>> GetAssemblyListAsync()
        {
            // TODO: Please use explicit declaration only.
            var folder = Windows.ApplicationModel.Package.Current.InstalledLocation;

            List<Assembly> assemblies = new List<Assembly>();
            foreach (Windows.Storage.StorageFile file in await folder.GetFilesAsync())
            {
                if (file.FileType == ".dll" || file.FileType == ".exe")
                {
                    AssemblyName name = new AssemblyName() { Name = global::System.IO.Path.GetFileNameWithoutExtension(file.Name) };
                    Assembly asm = Assembly.Load(name);
                    assemblies.Add(asm);
                }
            }

            return assemblies;
        }
    }
#endif

    /// <summary>Class AttributesProcessor.</summary>
    public class AttributesProcessor
    {
        /// <summary>The supported attributes.</summary>
        public static readonly List<Type> SupportedAttributes = new List<Type>
                                                                    {
                                                                        typeof(ECSEntitySystem),
                                                                        typeof(ECSEntityTemplate),
                                                                        typeof(ECSComponentPool),
                                                                        typeof(ECSComponentCreate)
                                                                    };

        /// <summary>Processes the specified supported attributes.</summary>
        /// <param name="supportedAttributes">The supported attributes.</param>
        /// <returns>The Dictionary{TypeList{Attribute}}.</returns>
        public static IDictionary<Type, List<Attribute>> Process(List<Type> supportedAttributes)
        {
            return Process(supportedAttributes, AppDomain.CurrentDomain.GetAssemblies());
        }

        /// <summary>Processes the specified supported attributes.</summary>
        /// <param name="supportedAttributes">The supported attributes.</param>
        /// <param name="assembliesToScan">The assemblies to scan.</param>
        /// <returns>The Dictionary{TypeList{Attribute}}.</returns>
        public static IDictionary<Type, List<Attribute>> Process(List<Type> supportedAttributes, IEnumerable<Assembly> assembliesToScan) // Do not double overload "= null)"
        {
            IDictionary<Type, List<Attribute>> attributeTypes = new Dictionary<Type, List<Attribute>>();

            if (assembliesToScan != null)
            {
                foreach (Assembly item in assembliesToScan)
                {

                    IEnumerable<Type> types = item.GetTypes();

                    foreach (Type type in types)
                    {

                        object[] attributes = type.GetCustomAttributes(false);

                        foreach (object attribute in attributes)
                        {
                            if (supportedAttributes.Contains(attribute.GetType()))
                            {
                                if (!attributeTypes.ContainsKey(type))
                                {
                                    attributeTypes[type] = new List<Attribute>();
                                }

                                attributeTypes[type].Add((Attribute)attribute);
                            }
                        }
                    }
                }
            }

            return attributeTypes;
        }
    }
}
