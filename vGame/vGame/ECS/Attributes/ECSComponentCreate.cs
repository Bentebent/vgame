﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vGame.ECS.Attributes
{
    /// <summary>Class ArtemisComponentCreate.</summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    public sealed class ECSComponentCreate : Attribute
    {
    }
}
