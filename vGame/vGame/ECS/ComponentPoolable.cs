﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using vGame.ECS.Interface;

namespace vGame.ECS
{
    /// <summary>Class ComponentPool-able.</summary>
    public abstract class ComponentPoolable : IComponent
    {
        /// <summary>Initializes a new instance of the <see cref="ComponentPoolable"/> class.</summary>
        protected ComponentPoolable()
        {
            this.PoolId = 0;
        }

        /// <summary>Gets or sets the pool id.</summary>
        /// <value>The pool id.</value>
        internal int PoolId { get; set; }

        /// <summary>Cleans up.</summary>
        public virtual void CleanUp()
        {
        }

        /// <summary>Initializes this instance.</summary>
        public virtual void Initialize()
        {
        }
    }
}
