﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using vGame.ECS.Interface;

namespace vGame.ECS.Manager
{
    /// <summary>Delegate RemovedComponentHandler.</summary>
    /// <param name="entity">The entity.</param>
    /// <param name="component">The component.</param>
    public delegate void RemovedComponentHandler(Entity entity, IComponent component);
}
