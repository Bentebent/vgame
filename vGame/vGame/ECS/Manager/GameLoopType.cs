﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vGame.ECS.Manager
{
    /// <summary>The game loop type.</summary>
    public enum GameLoopType
    {
        /// <summary>The update.</summary>
        Update,

        /// <summary>The draw.</summary>
        Draw
    }    
}
