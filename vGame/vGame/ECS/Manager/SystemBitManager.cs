﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;

using vGame.ECS.System;

namespace vGame.ECS.Manager
{
    /// <summary>Class SystemBitManager.</summary>
    internal class SystemBitManager
    {
        /// <summary>The system bits.</summary>
        private readonly Dictionary<EntitySystem, BigInteger> systemBits = new Dictionary<EntitySystem, BigInteger>();

        /// <summary>The position.</summary>
        private int position;

        /// <summary>Gets the bit-register for the specified entity system.</summary>
        /// <param name="entitySystem">The entity system.</param>
        /// <returns>The bit flag register for the specified system.</returns>
        public BigInteger GetBitFor(EntitySystem entitySystem)
        {
            BigInteger bit;
            if (this.systemBits.TryGetValue(entitySystem, out bit) == false)
            {
#if WINDOWS_PHONE || XBOX || PORTABLE || FORCEINT32
                if (this.position == 32)
                {
                    // bit is going to overflow and become 1 again
                    throw new InvalidOperationException("EntitySystem instances limit reached: number of EntitySystem instances is restricted to 32 in the current Artemis build.");
                }

                bit = 1 << this.position;
#else
                bit = 1L << this.position;
#endif
                this.position++;
                this.systemBits.Add(entitySystem, bit);
            }

            return bit;
        }
    }
}
