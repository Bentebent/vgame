﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using vGame.ECS.Interface;

namespace vGame.ECS.Manager
{
    /// <summary>Delegate AddedComponentHandler.</summary>
    /// <param name="entity">The entity.</param>
    /// <param name="component">The component.</param>
    public delegate void AddedComponentHandler(Entity entity, IComponent component);
}
