﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vGame.ECS.Manager
{
    /// <summary>Delegate AddedEntityHandler.</summary>
    /// <param name="entity">The entity.</param>
    public delegate void AddedEntityHandler(Entity entity);
}
