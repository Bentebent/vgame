﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vGame.ECS.Manager
{
    /// <summary>Delegate RemovedEntityHandler.</summary>
    /// <param name="entity">The entity.</param>
    public delegate void RemovedEntityHandler(Entity entity);
}
