﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vGame.ECS.Manager
{
    /// <summary>Enumeration ExecutionType.</summary>
    public enum ExecutionType
    {
        /// <summary>The synchronous.</summary>
        Synchronous,

        /// <summary>The asynchronous.</summary>
        Asynchronous,
    }
}
