﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Text;

namespace vGame.ECS.Exceptions
{
    [Serializable]
    public class MissingEntityTemplateException :Exception
    {
        /// <summary>Initializes a new instance of the <see cref="MissingEntityTemplateException" /> class.</summary>
        /// <param name="entityTemplateTag">The entity template tag.</param>
        internal MissingEntityTemplateException(string entityTemplateTag)
            : this(entityTemplateTag, null)
        {
        }

        /// <summary>Initializes a new instance of the <see cref="MissingEntityTemplateException" /> class.</summary>
        /// <param name="entityTemplateTag">The entity template tag.</param>
        /// <param name="inner">The inner.</param>
        internal MissingEntityTemplateException(string entityTemplateTag, Exception inner)
            : base("EntityTemplate for the tag " + entityTemplateTag + " was not registered.", inner)
        {
        }

        /// <summary>Initializes a new instance of the <see cref="MissingEntityTemplateException" /> class.</summary>
        /// <param name="info">The serialization info.</param>
        /// <param name="context">The serialization context.</param>
        protected MissingEntityTemplateException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
