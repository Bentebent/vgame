﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using System.Linq;
using System.Text;

using vGame.ECS.Interface;
using vGame.ECS.Manager;

namespace vGame.ECS
{
    /// <summary>Represents a Component Type.</summary>
    [DebuggerDisplay("Id:{Id}, Bit:{Bit}")]
    public sealed class ComponentType
    {
        /// <summary>The bit next instance of the <see cref="ComponentType"/> class will get.</summary>
        private static BigInteger nextBit;

        /// <summary>The id next instance of the <see cref="ComponentType"/> class will get.</summary>
        private static int nextId;

        /// <summary>Initializes static members of the <see cref="ComponentType"/> class.</summary>
        static ComponentType()
        {
            nextBit = 1;
            nextId = 0;
        }

        /// <summary>Initializes a new instance of the <see cref="ComponentType"/> class.</summary>
        internal ComponentType()
        {

            this.Id = nextId;
            this.Bit = nextBit;

            nextId++;
            nextBit <<= 1;
        }

        /// <summary>Gets the bit index that represents this type of component.</summary>
        /// <value>The id.</value>
        public int Id { get; private set; }

        /// <summary>Gets the bit that represents this type of component.</summary>
        /// <value>The bit.</value>
        public BigInteger Bit { get; private set; }
    }

    /// <summary>The component type class.</summary>
    /// <typeparam name="T">The Type T.</typeparam>
    internal static class ComponentType<T> where T : IComponent
    {
        /// <summary>Initializes static members of the <see cref="ComponentType{T}"/> class.</summary>
        static ComponentType()
        {
            CType = ComponentTypeManager.GetTypeFor<T>();
            if (CType == null)
            {
                CType = new ComponentType();
                ComponentTypeManager.SetTypeFor<T>(CType);
            }
        }

        /// <summary>Gets the type of the C.</summary>
        /// <value>The type of the C.</value>
        public static ComponentType CType { get; private set; }
    }
}
