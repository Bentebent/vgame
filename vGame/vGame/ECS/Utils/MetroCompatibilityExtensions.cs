﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if METRO
namespace vGame.ECS.Utils
{
    #region Using statements

    using global::System;

    using global::System.Reflection;

    #endregion

    /// <summary>The metro compatibility extensions.</summary>
    internal static class MetroCompatibilityExtensions
    {
        /// <summary>The create delegate.</summary>
        /// <param name="self">The self.</param>
        /// <param name="type">The type.</param>
        /// <returns>The <see cref="Delegate" />.</returns>
        public static Delegate CreateDelegate(this MethodInfo self, Type type)
        {
            return self.CreateDelegate(type);
        }
    }
}
#endif