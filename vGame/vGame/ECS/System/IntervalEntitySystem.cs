﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using vGame.ECS.Utils;

namespace vGame.ECS.System
{
    /// <summary>Class IntervalEntitySystem.</summary>
    public abstract class IntervalEntitySystem : EntitySystem
    {
        /// <summary>The timer.</summary>
        private readonly Timer timer;

        /// <summary>Initializes a new instance of the <see cref="IntervalEntitySystem"/> class.</summary>
        /// <param name="timeSpan">The time span.</param>
        /// <param name="types">The types.</param>
        protected IntervalEntitySystem(TimeSpan timeSpan, params Type[] types)
            : base(types)
        {
            this.timer = new Timer(timeSpan);
        }

        /// <summary>Initializes a new instance of the <see cref="IntervalEntitySystem"/> class.</summary>
        /// <param name="timeSpan">The time span.</param>
        /// <param name="aspect">The aspect.</param>
        protected IntervalEntitySystem(TimeSpan timeSpan, Aspect aspect)
            : base(aspect)
        {
            this.timer = new Timer(timeSpan);
        }

        /// <summary>Checks the processing.</summary>
        /// <returns><see langword="true" /> if this instance is enabled, <see langword="false" /> otherwise</returns>
        protected override bool CheckProcessing()
        {
            if (this.timer.IsReached(this.EntityWorld.Delta))
            {
                return this.IsEnabled;
            }

            return false;
        }
    }
}
