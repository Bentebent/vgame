﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using vGame.ECS.Utils;

namespace vGame.ECS.System
{
    /// <summary>Class IntervalTagSystem.</summary>
    public abstract class IntervalTagSystem : TagSystem
    {
        /// <summary>The timer.</summary>
        private readonly Timer timer;

        /// <summary>Initializes a new instance of the <see cref="IntervalTagSystem"/> class.</summary>
        /// <param name="timeSpan">The time span.</param>
        /// <param name="tag">The tag.</param>
        protected IntervalTagSystem(TimeSpan timeSpan, string tag)
            : base(tag)
        {
            this.timer = new Timer(timeSpan);
        }

        /// <summary>Checks the processing.</summary>
        /// <returns><see langword="true" /> if this instance is enabled, <see langword="false" /> otherwise</returns>
        protected override bool CheckProcessing()
        {
            if (this.timer.IsReached(this.EntityWorld.Delta))
            {
                return this.IsEnabled;
            }

            return false;
        }
    }
}
