﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vGame.ECS.System
{
    /// <summary><para>The processing system class.</para>
    /// <para>Special type of System that has NO entity associated (called once each frame).</para>
    /// <para>Extend it and override the ProcessSystem function.</para></summary>
    public abstract class ProcessingSystem : EntitySystem
    {
        /// <summary>Called when [added].</summary>
        /// <param name="entity">The entity.</param>
        public override void OnAdded(Entity entity)
        {
        }

        /// <summary>Called when [change].</summary>
        /// <param name="entity">The entity.</param>
        public override void OnChange(Entity entity)
        {
        }

        /// <summary>Called when [disabled].</summary>
        /// <param name="entity">The entity.</param>
        public override void OnDisabled(Entity entity)
        {
        }

        /// <summary>Called when [enabled].</summary>
        /// <param name="entity">The entity.</param>
        public override void OnEnabled(Entity entity)
        {
        }

        /// <summary>Called when [removed].</summary>
        /// <param name="entity">The entity.</param>
        public override void OnRemoved(Entity entity)
        {
        }

        /// <summary>Processes this instance. [Internal]</summary>
        public override void Process()
        {
            if (this.CheckProcessing())
            {
                this.Begin();
                this.ProcessSystem();
                this.End();
            }
        }

        /// <summary>Processes the System. Users must extend this method. Called once per frame.</summary>
        public abstract void ProcessSystem();

        /// <summary>Processes the specified entity.</summary>
        /// <param name="entity">The entity.</param>
        public void Process(Entity entity)
        {
        }
    }
}
