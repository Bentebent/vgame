﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vGame.ECS.System
{
    /// <summary>The entity processing system - a template for processing many entities, tied to components.</summary>
    public abstract class EntityProcessingSystem : EntitySystem
    {
        /// <summary>Initializes a new instance of the <see cref="EntityProcessingSystem" /> class.</summary>
        /// <param name="aspect">The aspect.</param>
        protected EntityProcessingSystem(Aspect aspect)
            : base(aspect)
        {
        }

        /// <summary>Initializes a new instance of the <see cref="EntityProcessingSystem"/> class.</summary>
        /// <param name="requiredType">The required Type.</param>
        /// <param name="otherTypes">The optional other types.</param>
        protected EntityProcessingSystem(Type requiredType, params Type[] otherTypes)
            : base(EntitySystem.GetMergedTypes(requiredType, otherTypes))
        {
        }

        /// <summary><para>Processes the specified entity.</para>
        /// <para>Users might extend this method when they want</para>
        /// <para>to process the specified entities.</para></summary>
        /// <param name="entity">The entity.</param>
        public abstract void Process(Entity entity);

        /// <summary>Processes the entities.</summary>
        /// <param name="entities">The entities.</param>
        protected override void ProcessEntities(IDictionary<int, Entity> entities)
        {
            foreach (Entity entity in entities.Values)
            {
                this.Process(entity);
            }
        }
    }
}
