﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using vGame.ECS.Manager;

namespace vGame.ECS.System
{
    /// <summary>Class HybridQueueSystemProcessing.</summary>
    public abstract class HybridQueueSystemProcessing : EntityProcessingSystem
    {
        /// <summary>The entities to process each frame.</summary>
        public const int EntitiesToProcessEachFrame = 50;

        /// <summary>The comp types.</summary>
        private readonly List<ComponentType> compTypes;

        /// <summary>The queue.</summary>
        private readonly Queue<Entity> queue;

        /// <summary>Initializes a new instance of the <see cref="HybridQueueSystemProcessing" /> class.</summary>
        /// <param name="requiredType">Type of the required.</param>
        /// <param name="otherTypes">The other types.</param>
        protected HybridQueueSystemProcessing(Type requiredType, params Type[] otherTypes)
            : base(requiredType, otherTypes)
        {
            this.queue = new Queue<Entity>();
            this.compTypes = new List<ComponentType>();
            foreach (Type type in this.Types)
            {
                this.compTypes.Add(ComponentTypeManager.GetTypeFor(type));
            }
        }

        /// <summary>Gets the queue count.</summary>
        /// <value>The queue count.</value>
        public int QueueCount
        {
            get
            {
                return this.queue.Count;
            }
        }

        /// <summary>Adds to queue.</summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="Exception">This EntitySystem does not process this kind of entity</exception>
        public void AddToQueue(Entity entity)
        {
            if (!this.Interests(entity))
            {
                throw new Exception("This EntitySystem does not process this kind of entity.");
            }

            this.queue.Enqueue(entity);
        }

        /// <summary>Processes the entities.</summary>
        /// <param name="entities">The entities.</param>
        protected override void ProcessEntities(IDictionary<int, Entity> entities)
        {
            if (!this.IsEnabled)
            {
                return;
            }

            int size = this.queue.Count > EntitiesToProcessEachFrame ? EntitiesToProcessEachFrame : this.queue.Count;
            for (int index = 0; index < size; ++index)
            {
                this.Process(this.queue.Dequeue());
            }

            base.ProcessEntities(entities);
        }
    }
}
