﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vGame.ECS.System
{
    /// <summary>Class IntervalEntityProcessingSystem.</summary>
    public abstract class IntervalEntityProcessingSystem : IntervalEntitySystem
    {
        /// <summary>Initializes a new instance of the <see cref="IntervalEntityProcessingSystem"/> class.</summary>
        /// <param name="timeSpan">The time span.</param>
        /// <param name="requiredType">Type of the required.</param>
        /// <param name="otherTypes">The other types.</param>
        protected IntervalEntityProcessingSystem(TimeSpan timeSpan, Type requiredType, params Type[] otherTypes)
            : base(timeSpan, EntitySystem.GetMergedTypes(requiredType, otherTypes))
        {
        }

        /// <summary>Initializes a new instance of the <see cref="IntervalEntityProcessingSystem" /> class.</summary>
        /// <param name="timeSpan">The time span.</param>
        /// <param name="aspect">The aspect.</param>
        protected IntervalEntityProcessingSystem(TimeSpan timeSpan, Aspect aspect)
            : base(timeSpan, aspect)
        {
        }

        /// <summary>Processes the specified entity.</summary>
        /// <param name="entity">The entity.</param>
        public abstract void Process(Entity entity);

        /// <summary>Processes the entities.</summary>
        /// <param name="entities">The entities.</param>
        protected override void ProcessEntities(IDictionary<int, Entity> entities)
        {
            foreach (Entity entity in entities.Values)
            {
                this.Process(entity);
            }
        }
    }
}
