﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using vGame.ECS;
using vGame.ECS.Attributes;
using vGame.ECS.Manager;
using vGame.ECS.System;
using vGame.ECS.Utils;

using vGame.Components;

namespace vGame.Systems
{
    [ECSEntitySystem(GameLoopType = GameLoopType.Update, Layer = 0)]
    internal class TestSystem : EntitySystem
    {
        private ComponentMapper<TestComponent> m_componentMapper;

        public TestSystem()
            :base(typeof(TestComponent))
        {

        }

        public override void LoadContent()
        {
            m_componentMapper = new ComponentMapper<TestComponent>(this.EntityWorld);
        }
        
        protected override void ProcessEntities(IDictionary<int, Entity> entities)
        {
            foreach (KeyValuePair<int, Entity> entity in entities)
            {
                TestComponent tc = m_componentMapper.Get(entity.Value);
                tc.position += new Vector2(1.0f, 1.0f);
            }
           
           
            int lastCount = entities.Keys.ElementAt(0);
            Entity e = entities[lastCount];
            e.Delete();

            base.ProcessEntities(entities);
        }
    }
}
