using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using vGame.ECS;
using vGame.ECS.System;

using vGame.Components;


namespace vGame
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private EntityWorld m_entityWorld;

        private SpriteFont  m_debugFont;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this)
                {
                    IsFullScreen = false,
                    PreferredBackBufferHeight = 720,
                    PreferredBackBufferWidth = 1280,
                    PreferredBackBufferFormat = SurfaceFormat.Color,
                    PreferMultiSampling = false,
                    PreferredDepthStencilFormat = DepthFormat.None
                };

            IsFixedTimeStep = false;

            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            m_entityWorld = new EntityWorld();


            EntitySystem.BlackBoard.SetEntry("ContentManager", Content);
            EntitySystem.BlackBoard.SetEntry("GraphicsDevice", GraphicsDevice);
            EntitySystem.BlackBoard.SetEntry("SpriteBatch", spriteBatch);
            EntitySystem.BlackBoard.SetEntry("SpriteFont", m_debugFont);
            EntitySystem.BlackBoard.SetEntry("EnemyInterval", 500);

            m_entityWorld.InitializeAll(true);

            for(int i = 0; i < 10000; i++)
            {
                Entity e = m_entityWorld.CreateEntity();
                e.Group = "TEST";
                e.Tag = i.ToString(); //MUST BE UNIQUE

                e.AddComponent(new TestComponent());
                e.GetComponent<TestComponent>().position = new Vector2(0, 0);
                e.Refresh();
            }

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            m_debugFont = Content.Load<SpriteFont>("myFont");


            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape) ||
               GamePad.GetState(PlayerIndex.One).IsButtonDown(Buttons.Back))
            {
                this.Exit();
            }

            m_entityWorld.Update();

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            m_entityWorld.Draw();


#if DEBUG
            string entityCount = string.Format("Active entities: {0}", m_entityWorld.EntityManager.ActiveEntities.Count);
            string removedEntityCount = string.Format("Removed entities: {0}", m_entityWorld.EntityManager.TotalRemoved);
            string totalEntityCount = string.Format("Total entities: {0}", m_entityWorld.EntityManager.TotalCreated);

            spriteBatch.DrawString(m_debugFont, entityCount, new Vector2(32, 62), Color.Yellow);
            spriteBatch.DrawString(m_debugFont, removedEntityCount, new Vector2(32, 92), Color.Yellow);
            spriteBatch.DrawString(m_debugFont, totalEntityCount, new Vector2(32, 122), Color.Yellow);
#endif

           spriteBatch.End(); 
           base.Draw(gameTime);
        }
    }
}
